Este código foi baseado no seguinte artigo: 
  Shen, Jing, Jian Wan, Se-Jung Lim, and Lifeng Yu. “Random-Forest-Based Failure Prediction for Hard Disk Drives.” International Journal of Distributed Sensor Networks, (November 2018). https://doi.org/10.1177/1550147718806480.

São dois notebooks, um com o tratamento de dados que irá retornar os dados de treino e de teste. O outro contém o algoritmo híbrido com o *K-Means* e o *Random Forest*.
